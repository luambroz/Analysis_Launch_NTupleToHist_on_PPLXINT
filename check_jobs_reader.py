import glob

completed = glob.glob('status/completed-*')
#done = glob.glob('status/done-*')

c_numbers = []

for c in completed:
  c_numbers.append(int(c.split("-")[1]))

c_numbers.sort()
f = open('resubmit.sh', 'w')
f.write('#!/bin/bash \n')
for i in range(0, c_numbers[-1]+1):
  if i not in c_numbers:
    print(str(i)) #Missing
    f.write('./submit/run ' + str(i) + '\n')

f.close()