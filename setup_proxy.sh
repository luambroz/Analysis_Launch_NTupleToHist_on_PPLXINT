#!/bin/bash
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
localSetupRucioClients
voms-proxy-init -voms atlas -valid 96:00:00
cp $X509_USER_PROXY /home/${USER}/Scripts_download/myProxy