#PBS -q datatransfer
source /home/ambroz/Scripts_download/setup_dq2_cluster.sh
lsetup FAX
cd /home/ambroz/data3/VHbb/VHbbTruthFramework/ZplusJets_v2/361515
rucio download group.phys-higgs.mc15_13TeV.361515.MadGraphPythia8EvtGen_Znunu_Np0.evgen.EVNT.e3898.VHbb.BETA_v06_MG_NP_LUCA_0_Lep/
cd -
#To run it: qsub Quick_Download.sh