#!/bin/sh
#PBS -q datatransfer 

cd $PBS_O_WORKDIR
export KRB5CCNAME=${HOME}/.aklog_cache

echo $KRB5CCNAME

klist
tokens

#scp luambroz@lxplus.cern.ch:~/test.txt /home/ambroz/Scripts_download
#rsync -Pav -e ssh luambroz@lxplus.cern.ch:/eos/atlas/atlascerngroupdisk/phys-higgs/HSG5/Run2/VH/CxAOD_r31-10/HIGG5D1_13TeV /data/atlas/atlasdata3/ambroz/VHbb/CxAOD/CxAOD_r31-10
#rsync -Pav -e ssh luambroz@lxplus.cern.ch:/eos/atlas/atlascerngroupdisk/phys-higgs/HSG5/Run2/VH/CxAOD_r31-25/HIGG5D1_13TeV /data/atlas/atlasdata3/ambroz/VHbb/CxAOD/CxAOD_r31-25

#rsync flags:
#a -> archive. Allow to preserve simb links etc..
#v -> verbose
#P -> for progress
#-e ssh -> use ssh shell

#z -> to compress the files. Can speed things up
#--delete -> for a true syncronization
#n -> for a dry "trial" run
#HIGG5D1_13TeV does not end with / -> it means copy both the directory and the content 

#rsync -Pav -e ssh luambroz@lxplus.cern.ch:/eos/atlas/atlascerngroupdisk/phys-higgs/HSG5/Run2/VH/CxAOD_r32-02/HIGG5D1_13TeV /data/atlas/atlasdata3/ambroz/VHbb/CxAOD/CxAOD_r32-02
rsync -Pavz --delete  -e ssh luambroz@lxplus.cern.ch:/eos/atlas/atlascerngroupdisk/phys-higgs/HSG5/Run2/VH/CxAOD_r32-02/HIGG5D1_13TeV /data/atlas/atlasdata3/ambroz/VHbb/CxAOD/CxAOD_r32-02

#Do: qsub copy_from_lxplus.sh 
#qsub -l cput=11:59:59 -l walltime=11:59:59 copy_from_lxplus.sh
#You need to setup on pplxint correcty .ssh/config and also knit