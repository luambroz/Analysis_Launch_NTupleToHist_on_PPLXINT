#!/bin/bash
export X509_USER_PROXY=/home/${USER}/Scripts_download/myProxy
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
localSetupPandaClient
lsetup rucio
localSetupPyAMI
export DQ2_LOCAL_SITE_ID=UKI-SOUTHGRID-OX-HEP_SCRATCHDISK