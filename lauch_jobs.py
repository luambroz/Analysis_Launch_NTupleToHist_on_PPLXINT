#!/usr/bin/python

#######################################################################
#                                                                     #
# Script to lauch jobs on the Oxford cluster.                         #
# Author luca.ambroz@cern.ch                                          #
#                                                                     #
#######################################################################

import os
import glob
import time
import shutil

def main():

  #Options
  Automatically_merge = True
  Remove_messages = False
  Channel = 0 


  initial_folder = os.getcwd()

  samples = []

  #Signal 0Lep
  input_files345056 = glob.glob("/home/ambroz/data3/VHbb/VHbbTruthFramework/Signal0lep/345056/group.phys-higgs/*.ntuple.roo*")
  input_files345056nocutsWZ = glob.glob("/home/ambroz/data3/VHbb/VHbbTruthFramework/Signal0lep/345056nocuts/user.luambroz/*.ntuple.roo*")
  input_files345056nocutsTruthJets = glob.glob("/home/ambroz/data3/VHbb/VHbbTruthFramework/Signal0lep/345056nocuts_v2/user.luambroz/*.ntuple.roo*")

  #Z+jets (small DSID)
  input_files361515 = glob.glob("/home/ambroz/data3/VHbb/VHbbTruthFramework/ZplusJets/361515/group.phys-higgs/*.roo*")
  input_files361516 = glob.glob("/home/ambroz/data3/VHbb/VHbbTruthFramework/ZplusJets/361516/group.phys-higgs/*.roo*")
  input_files361517 = glob.glob("/home/ambroz/data3/VHbb/VHbbTruthFramework/ZplusJets/361517/group.phys-higgs/*.roo*")
  input_files361518 = glob.glob("/home/ambroz/data3/VHbb/VHbbTruthFramework/ZplusJets/361518/group.phys-higgs/*.roo*")
  input_files361519 = glob.glob("/home/ambroz/data3/VHbb/VHbbTruthFramework/ZplusJets/361519/group.phys-higgs/*.roo*")
  input_files364149 = glob.glob("/home/ambroz/data3/VHbb/VHbbTruthFramework/ZplusJets/364149/group.phys-higgs/*.roo*")
  
  #TTBar
  #input_files410501 = glob.glob("/home/ambroz/data3/VHbb/VHbbTruthFramework/TTbarAndy/group.phys-higgs.mc15_13TeV.410501.PowhegPythia8EvtGen_nonallhad.evgen.EVNT.e5458.VHbb.BETA_v01.SMEAR_LUCA_EXT0/*9.ntuple.root")
  #input_files410501_old = glob.glob("/home/ambroz/data3/VHbb/VHbbTruthFramework/TTbarAndy/group.phys-higgs.mc15_13TeV.410501.PowhegPythia8EvtGen_nonallhad.evgen.EVNT.e5458.VHbb.BETA_v01.SMEAR_LUCA_EXT0/*.ntuple.roo*")
  input_files410501 = glob.glob("/home/ambroz/data3/VHbb/VHbbTruthFramework/TTbarAndy/410501/group.phys-higgs/*.ntuple.roo*")
  input_files410511 = glob.glob("/home/ambroz/data3/VHbb/VHbbTruthFramework/TTbarAndy/410511/group.phys-higgs/*.ntuple.roo*")
  input_files410512 = glob.glob("/home/ambroz/data3/VHbb/VHbbTruthFramework/TTbarAndy/410512/group.phys-higgs/*.ntuple.roo*")
  input_files410225 = glob.glob("/home/ambroz/data3/VHbb/VHbbTruthFramework/TTbarAndy/410225/group.phys-higgs/*.ntuple.roo*")
  input_files410525 = glob.glob("/home/ambroz/data3/VHbb/VHbbTruthFramework/TTbarAndy/410525/group.phys-higgs/*.ntuple.roo*") 
  

  #Uncomment below the samples that you want to run on. Remember to set true the flag on makeHistograms for the batch submission.
  #samples.append(input_files345056)
  #samples.append(input_files345056nocutsWZ)
  #samples.append(input_files345056nocutsTruthJets)
  #samples.append(input_files361515)
  #samples.append(input_files361519)
  samples.append(input_files364149)
  #samples.append(input_files410501)
  #samples.append(input_files410511)
  #samples.append(input_files410512)
  #samples.append(input_files410225)
  #samples.append(input_files410525)  

  #######

  for input_files in samples:

    if '345056' in input_files[0]:
      DSID = 345056
    if '361515' in input_files[0]:
      DSID = 361515  
    if '361516' in input_files[0]:
      DSID = 361516
    if '361517' in input_files[0]:
      DSID = 361517
    if '361518' in input_files[0]:
      DSID = 361518
    if '361519' in input_files[0]:
      DSID = 361519
    if '364149' in input_files[0]:
      DSID = 364149
    if '410501' in input_files[0]:
      DSID = 410501
    if '410511' in input_files[0]:
      DSID = 410511
    if '410512' in input_files[0]:
      DSID = 410512
    if '410225' in input_files[0]:
      DSID = 410225
    if '410525' in input_files[0]:
      DSID = 410525  

    print('Running on DSID: ' + str(DSID))

    #####

    for input_file in input_files:
      #Careful! You lose the the files .root.2
      #Careful also with names like .ntuple.root
      #Job number. Return from position -4 to the end.
      filenumber = input_file.split(".root")[0][-4:]
      fName = 'Job_' + filenumber  + '.sh' 
      
      f = open(fName, 'w')
      f.write('#!/bin/bash \n')
      f.write('cd $TMPDIR \n') 
      f.write('source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh \n')
      f.write('workdir=$(pwd) \n')
      f.write('mkdir run/  \n')
      f.write('cp -r /home/ambroz/VHbb/VHbbTruthFramework_v2/source/ . \n')
      f.write('cp -r /home/ambroz/VHbb/VHbbTruthFramework_v2/build/ . \n')  
      f.write('cd build \n')
      f.write('asetup --restore \n')
      f.write('source x86_64-slc6-gcc62-opt/setup.sh \n')
      f.write('cd ../run \n')
      
      #Create an output directory (if it exists, remove it and create a new one).
      output_dir =  "/home/ambroz/VHbb/VHbbTruthFramework_v2/run/OutputDir" + str(DSID) + "/"
      if os.path.exists(output_dir):
        shutil.rmtree(output_dir)
      os.makedirs(output_dir)
   
      #Arg1: 0 Lep Analysis. Arg2: inputfile. Arg3: DSID. Arg4 OutpuDir (not necessary, you can copy them later). 
      f.write('NtupleToHist ' + str(Channel) + ' ' + input_file + ' ' + str(DSID) + ' ' + '\n')
      f.write('cp CorrsAndSyst*.root ' + output_dir + "fileCorrsAndSyst" + filenumber + '.root\n')
      f.write('cp output*.root ' + output_dir + "fileoutput" + filenumber + '.root\n')
      f.close() 

      #Create in this directory files for the output and error messages. The error message will always containing an error from setupATLAS for bashrc 
      message = " -o output" + filenumber + ".txt -e error" + filenumber + ".txt "
      
      #Submission
      os.system("qsub -l cput=01:59:59 -l walltime=01:59:59" + message + fName)
      os.remove(fName)  

    #####    

    #Automatic merge
    if(Automatically_merge == True):
      while(1):
        os.system("qstat -u ambroz > job_queue_ambroz.txt")
        #Check if job queue is empty.
        if(os.stat("job_queue_ambroz.txt").st_size == 0):
          os.remove("job_queue_ambroz.txt")
          os.chdir(output_dir)
          os.system("hadd CorrsAndSyst" + str(DSID) + ".root fileCorrsAndSyst*.root")
          os.system("rm fileCorrsAndSyst*.root")
          os.system("hadd result" + str(DSID) + ".root fileoutput*.root")
          os.system("rm fileoutput*.root")
          break
        os.remove("job_queue_ambroz.txt")    
        #Check every tow seconds if the jobs are done
        time.sleep(2)

    #Go back to initial folder
    os.chdir(initial_folder)

    #Remove error messages
    if(Remove_messages == True):
      os.system("rm error*.txt")
      os.system("rm output*.txt")  

    #Final message
    if(Automatically_merge == True):
      print("Jobs DSID " + str(DSID) + " are done!")

#######

if __name__ == '__main__':
  main()

