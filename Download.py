import sys
import os
import time

def main():

  #inputListFile is a .txt file
  #inputListFile   = '/home/ambroz/Scripts_download/410501.txt'
  #inputListFile   = '/home/ambroz/Scripts_download/410501codeTTBarDecay.txt'
  #inputListFile   = '/home/ambroz/Scripts_download/410511.txt'
  #inputListFile   = '/home/ambroz/Scripts_download/410512.txt'
  #inputListFile   = '/home/ambroz/Scripts_download/410225.txt'
  #inputListFile   = '/home/ambroz/Scripts_download/410525.txt'
  #inputListFile   = '/home/ambroz/Scripts_download/signalvvbb345056nocuts.txt'  
  #inputListFile   = '/home/ambroz/Scripts_download/signalvvbb345056nocuts_v2.txt'
  inputListFile   = '/home/ambroz/Scripts_download/signalvvbb345056nocuts_v3.txt' 
  #inputListFile   = '/home/ambroz/Scripts_download/410470.txt'
  #inputListFile   = '/home/ambroz/Scripts_download/410472.txt'
  #inputListFile   = '/home/ambroz/Scripts_download/410480.txt'
  #inputListFile   = '/home/ambroz/Scripts_download/410482.txt'
  #inputListFile   = '/home/ambroz/Scripts_download/410557.txt'
  #inputListFile   = '/home/ambroz/Scripts_download/410558.txt'
  #inputListFile   = '/home/ambroz/Scripts_download/361515.txt'

  #downloadDir     = '/home/ambroz/data3/VHbb/VHbbTruthFramework/TTbarAndy/410501/'
  #downloadDir     = '/home/ambroz/data3/VHbb/VHbbTruthFramework/TTbarAndy/410501codeTTBarDecay/'
  #downloadDir     = '/home/ambroz/data3/VHbb/VHbbTruthFramework/TTbarAndy/410511/'  
  #downloadDir     = '/home/ambroz/data3/VHbb/VHbbTruthFramework/TTbarAndy/410512/'
  #downloadDir     = '/home/ambroz/data3/VHbb/VHbbTruthFramework/TTbarAndy/410225/'
  #downloadDir     = '/home/ambroz/data3/VHbb/VHbbTruthFramework/TTbarAndy/410525/'
  #downloadDir     = '/home/ambroz/data3/VHbb/VHbbTruthFramework/TTbarAndy/410470/'
  #downloadDir     = '/home/ambroz/data3/VHbb/VHbbTruthFramework/TTbarAndy/410472/'
  #downloadDir     = '/home/ambroz/data3/VHbb/VHbbTruthFramework/TTbarAndy/410480/'
  #downloadDir     = '/home/ambroz/data3/VHbb/VHbbTruthFramework/TTbarAndy/410482/'
  #downloadDir     = '/home/ambroz/data3/VHbb/VHbbTruthFramework/TTbarAndy/410557/'
  #downloadDir     = '/home/ambroz/data3/VHbb/VHbbTruthFramework/TTbarAndy/410558/'
  #downloadDir     = '/home/ambroz/data3/VHbb/VHbbTruthFramework/Signal0lep/345056nocuts/'
  #downloadDir     = '/home/ambroz/data3/VHbb/VHbbTruthFramework/Signal0lep/345056nocuts_v2/'
  downloadDir     = '/home/ambroz/data3/VHbb/VHbbTruthFramework/Signal0lep/345056nocuts_v3_with_matching_WZ_jets_Valerio/'
  #downloadDir     ='/home/ambroz/data3/VHbb/VHbbTruthFramework/ZplusJets_v2/361515'

  #Z+Jets
  #DSIDs_Z_plus_jets_Madgraph = range(361500, 361520) #All
  #DSIDs_Z_plus_jets_Sherpa = range(364100, 364156) #All
  DSIDs_Z_plus_jets_Madgraph_for_0_Lep = range(361515, 361520) #Last one is excluded
  DSIDs_Z_plus_jets_Sherpa_for_0_Lep = range(364153, 364154)   #Last one is excluded 43 - 56

  #You can also pass the directory name from command line	
  if len(sys.argv) == 3:
	  inputListFile += sys.argv[1] 
	  downloadDir += sys.argv[2]

  #To download and clean (can be commented out) a single file
  #clean_download_file_group_h_production(inputListFile)
  download(inputListFile, downloadDir)

  #Download all the Z+jets
  #for DSID in DSIDs_Z_plus_jets_Sherpa_for_0_Lep:
    #Clean the files starting with group higgs
    #clean_download_file_group_h_production('/home/ambroz/Scripts_download/' + str(DSID) + '.txt')
    #inputListFile = '/home/ambroz/Scripts_download/' + str(DSID) + '.txt'
    #if os.path.exists(inputListFile) == False:
      #print("=====================================================")
      #print("Missing: " + inputListFile)
      #print("=====================================================")
      #continue
    #download(inputListFile, '/home/ambroz/data3/VHbb/VHbbTruthFramework/ZplusJets/' + str(DSID))


def download(inputListFile,downloadDir):

  print 'Read grid job output list from file : ', inputListFile
  print 'Check if downloadDir (',downloadDir,') exist :' , os.path.exists(downloadDir)
  if os.path.exists(downloadDir) == False:
    sys.exit("ERROR :: Download directory doesn't exist!!")

  print ' '
  count = 0

  datasetList = open(inputListFile).read().splitlines()

  for dataset in datasetList:
    if dataset.startswith('#'):
      print 'skipping : ' + dataset
      continue
    # Make a (temporary) batch job script (for each container)
    #
    print 'Send datatransfer batchjob for : ',dataset

    fName = 'download_files_' + str(count) + '.sh'
    f = open(fName, 'w')
    #f.write( '#!/bin/sh\n' )
    f.write('#PBS -q datatransfer\n')
    f.write('\n')
    f.write('source /home/ambroz/Scripts_download/setup_dq2_cluster.sh\n')
    f.write('lsetup FAX\n')
    f.write('cd ' + downloadDir + ' \n')
    f.write('echo Will download in this directory \n')
    f.write('pwd\n')
    f.write('rucio download ' + dataset + '\n')
    f.close()

    # Send script to the batch farm and delete the script
    #
    os.system('qsub ' + fName)
    os.remove(fName)
    count += 1
    print ' '

def clean_download_file_group_h_production(inputListFile):

  if os.path.exists(inputListFile) == False:
    return
    

  cleanlines = []

  with open(inputListFile, 'r') as handle:
    for line in handle:
      if line.startswith('|----'): 
        continue
      if line.startswith('+----'): 
        continue
      if line.startswith('| SCOPE:NAME '): 
        continue
      if line.startswith('Total'):
        continue
      if line.startswith('group.phys-higgs'):
        cleanlines.append(line) #Line already cleaned
      else: 
        cleanlines.append(line.split('| group.phys-higgs:')[1].split(' | ')[0] + '\n')

  #Erase old content of inputListFile
  open(inputListFile, 'w').close()
 
  with open(inputListFile, 'w') as handle:
    for line in cleanlines:
      handle.write(line)
    
def clean_download_file_Luca():    

  cleanlines = []

  with open(inputListFile, 'r') as handle:
    for line in handle:
      if line.startswith('|----'):
        continue
      if line.startswith('+----'):
        continue
      if line.startswith('| SCOPE:NAME '):
        continue
      cleanlines.append(line.split('| user.luambroz:')[1].split(' | CONTAINER    |')[0] + '\n')

  #Erase old content of inputListFile
  open(inputListFile, 'w').close()
 
  with open(inputListFile, 'w') as handle:
    for line in cleanlines:
      handle.write(line)

# # # # # # # # # # # # # # #

def Chunks( l, n ):
  return [ l[ i : i + n ] for i in range( 0, len( l ), n ) ]

# # # # # # # # # # # # # # #


if __name__ == '__main__':
  main()