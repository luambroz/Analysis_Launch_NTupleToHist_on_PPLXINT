#rucio list-dids user.nwhallon.data15_13TeV.002*.physics_Main.HIGG5D1.28-04_CxAOD.root | sort -n > grid_HighPtNtuple.txt
#rucio list-dids user.nwhallon.data16_13TeV.00*.physics_Main.HIGG5D1.28-04_CxAOD.root | sort -n > grid_HighPtNtuple.txt
#rucio list-dids user.nwhallon.mc15_13TeV.3641*.Sh_221_NNPDF30NNLO_Wenu_MAXHTPTV*_*_BFilter.s2726.HIGG5D1.28-*_CxAOD.root | sort -n > grid_HighPtNtuple.txt
#rucio list-dids user.nwhallon.mc15_13TeV.3*.Sh_221_NNPDF30NNLO_Wenu_MAXHTPTV*_*_CFilterBVeto.s2726.HIGG5D1.28-02_CxAOD.root | sort -n > grid_HighPtNtuple.txt
#rucio list-dids user.nwhallon.mc15_13TeV.36*.Sh_221_NNPDF30NNLO_Wenu_MAXHTPTV*_*_CVetoBVeto.s2726.HIGG5D1.28-02_CxAOD.root | sort -n > grid_HighPtNtuple.txt
#rucio list-dids user.nwhallon.mc15_13TeV.36*.Sh_221_NNPDF30NNLO_Wenu_MAXHTPTV*HIGG5D1.28-02_CxAOD.root | sort -n > grid_HighPtNtuple.txt
#python organize_download_files.py

#rucio list-dids user.luambroz.mc15*v00-06-0*_tuple.root | sort -n > grid_HighPtNtuple.txt
#rucio list-dids user.luambroz.data15*v00-06-01*_tuple.root | sort -n > grid_HighPtNtuple.txt
#rucio list-dids user.luambroz.data16*v00-06-01*_tuple.root | sort -n > grid_HighPtNtuple.txt
#rucio list-dids user.kliu.mc16_13TeV.345053.PwPy8EG_NNPDF3_AZNLO_WmH125J_MINLO_lvbb_VpT.e5984.HIGG5D2.v0002_CxAOD.root | sort -n > grid_HighPtNtuple.txt

#Truth Analysis Samples
#Ttbar old
#rucio list-files group.phys-higgs.mc15_13TeV.410501.PowhegPythia8EvtGen_ttbar_hdamp258p75_nonallhad.evgen.EVNT.e5458.VHbb.BETA_v02_EXT0 | sort -n > 410501.txt
#rucio list-files group.phys-higgs.mc15_13TeV.410501.PowhegPythia8EvtGen_nonallhad.evgen.EVNT.e5458.VHbb.BETA_v05.NOSMEAR_TTBAR_FLAVS_EXT0 | sort -n > 410501codeTTBarDecay.txt
#rucio list-files group.phys-higgs.mc15_13TeV.410511.PowhegPythia8EvtGen_ttbar_hdamp517p5_nonallhad.evgen.EVNT.e5556.VHbb.BETA_v02_EXT0 | sort -n > 410511.txt
#rucio list-files group.phys-higgs.mc15_13TeV.410512.PowhegPythia8EvtGen_ttbar_hdamp258p75_nonallhad.evgen.EVNT.e5556.VHbb.BETA_v02_EXT0 | sort -n > 410512.txt
#rucio list-files group.phys-higgs.mc15_13TeV.410225.aMcAtNloPythia8EvtGen_A14N23LO_ttbar_nonallhad.evgen.EVNT.e5465.VHbb.BETA_v02_EXT0 | sort -n > 410225.txt
#rucio list-files group.phys-higgs.mc15_13TeV.410525.PowhegHerwig7EvtGen_H7UE_tt_hdamp258p75_nonallhad.evgen.EVNT.e5903.VHbb.BETA_v02_EXT0 | sort -n > 410525.txt
#Ttbar new
#rucio list-files group.phys-higgs.mc16_13TeV.410480.PhPy8EG_ttbar_hdamp517p5_SingleLep.merge.EVNT.e6454_e5984.VHbb.BETA_v07_ttbar_0_Lep/ | sort -n > 410480.txt
#rucio list-files group.phys-higgs.mc16_13TeV.410482.PhPy8EG_A14_ttbar_hdamp517p5_dil.merge.EVNT.e6454_e5984.VHbb.BETA_v07_ttbar_0_Lep/ | sort -n > 410482.txt
#rucio list-files group.phys-higgs.mc16_13TeV.410558.PowhegHerwig7EvtGen_704_dil.merge.EVNT.e6366_e5984.VHbb.BETA_v07_ttbar_0_Lep/ | sort -n > 410558.txt
#rucio list-files group.phys-higgs.mc16_13TeV.410557.PowhegHerwig7EvtGen_704_SingleLep.merge.EVNT.e6366_e5984.VHbb.BETA_v07_ttbar_0_Lep/ | sort -n > 410557.txt
#rucio list-files group.phys-higgs.mc16_13TeV.410470.PhPy8EG_ttbar_hdamp258p75_nonallhad.merge.EVNT.e6337_e5984.VHbb.BETA_v07_ttbar_0_Lep/ | sort -n > 410470.txt
#rucio list-files group.phys-higgs.mc16_13TeV.410472.PhPy8EG_A14_ttbar_hdamp258p75_dil.merge.EVNT.e6348_e5984.VHbb.BETA_v07_ttbar_0_Lep/ | sort -n > 410472.txt

#Signal
#rucio list-files group.phys-higgs.mc15_13TeV.345056.PowhegPythia8EvtGen_vvbb_VpT.evgen.EVNT.e5706.VHbb.BETA_v05.NOSMEAR_ZvvH_SYST_EXT0 | sort -n > signalvvbb345056.txt
#rucio list-files user.luambroz:user.luambroz.mc15_13TeV.345056.PowhegPythia8EvtGen_ZH125J_MINLO_vvbb_VpT.evgen.EVNT.e5706signal345056nocutsv02_EXT0 | sort -n > signalvvbb345056nocuts_v2.txt
#rucio list-files user.vdao.mc15_13TeV.345056.PowhegPythia8EvtGen_NNPDF3_AZNLO_ZH125J_MINLO_vvbb_VpT.evgen.EVNT.e5706.VHbb._VD_v00_ntuple | sort -n > signalvvbb345056nocuts_v3.txt

#Z+Jets MadGraph
rucio list-files group.phys-higgs.mc15_13TeV.361519.MadGraphPythia8EvtGen_Znunu_Np4.evgen.EVNT.e3898.VHbb.BETA_v06_MG_NP_LUCA_0_Lep/ | sort -n > 361519.txt
rucio list-files group.phys-higgs.mc15_13TeV.361518.MadGraphPythia8EvtGen_Znunu_Np3.evgen.EVNT.e3898.VHbb.BETA_v06_MG_NP_LUCA_0_Lep/ | sort -n > 361518.txt
rucio list-files group.phys-higgs.mc15_13TeV.361517.MadGraphPythia8EvtGen_Znunu_Np2.evgen.EVNT.e3898.VHbb.BETA_v06_MG_NP_LUCA_0_Lep/ | sort -n > 361517.txt
rucio list-files group.phys-higgs.mc15_13TeV.361516.MadGraphPythia8EvtGen_Znunu_Np1.evgen.EVNT.e3898.VHbb.BETA_v06_MG_NP_LUCA_0_Lep/ | sort -n > 361516.txt
rucio list-files group.phys-higgs.mc15_13TeV.361515.MadGraphPythia8EvtGen_Znunu_Np0.evgen.EVNT.e3898.VHbb.BETA_v06_MG_NP_LUCA_0_Lep/ | sort -n > 361515.txt

#rucio list-files group.phys-higgs.mc15_13TeV.361514.MadGraphPythia8EvtGen_Ztautau_Np4.evgen.EVNT.e3898.VHbb.BETA_v06_MG_VJETS_0_Lep/ | sort -n > 361514.txt
#rucio list-files group.phys-higgs.mc15_13TeV.361513.MadGraphPythia8EvtGen_Ztautau_Np3.evgen.EVNT.e3898.VHbb.BETA_v06_MG_VJETS_0_Lep/ | sort -n > 361513.txt
#rucio list-files group.phys-higgs.mc15_13TeV.361512.MadGraphPythia8EvtGen_Ztautau_Np2.evgen.EVNT.e3898.VHbb.BETA_v06_MG_VJETS_0_Lep/ | sort -n > 361512.txt
#rucio list-files group.phys-higgs.mc15_13TeV.361511.MadGraphPythia8EvtGen_Ztautau_Np1.evgen.EVNT.e3898.VHbb.BETA_v06_MG_VJETS_0_Lep/ | sort -n > 361511.txt
#rucio list-files group.phys-higgs.mc15_13TeV.361510.MadGraphPythia8EvtGen_Ztautau_Np0.evgen.EVNT.e3898.VHbb.BETA_v06_MG_VJETS_0_Lep/ | sort -n > 361510.txt
#rucio list-files group.phys-higgs.mc15_13TeV.361509.MadGraphPythia8EvtGen_Zmumu_Np4.evgen.EVNT.e3898.VHbb.BETA_v06_MG_VJETS_0_Lep/   | sort -n > 361509.txt
#rucio list-files group.phys-higgs.mc15_13TeV.361508.MadGraphPythia8EvtGen_Zmumu_Np3.evgen.EVNT.e3898.VHbb.BETA_v06_MG_VJETS_0_Lep/   | sort -n > 361508.txt
#rucio list-files group.phys-higgs.mc15_13TeV.361507.MadGraphPythia8EvtGen_Zmumu_Np2.evgen.EVNT.e3898.VHbb.BETA_v06_MG_VJETS_0_Lep/   | sort -n > 361507.txt
#rucio list-files group.phys-higgs.mc15_13TeV.361506.MadGraphPythia8EvtGen_Zmumu_Np1.evgen.EVNT.e3898.VHbb.BETA_v06_MG_VJETS_0_Lep/   | sort -n > 361506.txt
#rucio list-files group.phys-higgs.mc15_13TeV.361505.MadGraphPythia8EvtGen_Zmumu_Np0.evgen.EVNT.e3898.VHbb.BETA_v06_MG_VJETS_0_Lep/   | sort -n > 361505.txt
#rucio list-files group.phys-higgs.mc15_13TeV.361504.MadGraphPythia8EvtGen_Zee_Np4.evgen.EVNT.e3898.VHbb.BETA_v06_MG_VJETS_0_Lep/     | sort -n > 361504.txt
#rucio list-files group.phys-higgs.mc15_13TeV.361503.MadGraphPythia8EvtGen_Zee_Np3.evgen.EVNT.e3898.VHbb.BETA_v06_MG_VJETS_0_Lep/     | sort -n > 361503.txt
#rucio list-files group.phys-higgs.mc15_13TeV.361502.MadGraphPythia8EvtGen_Zee_Np2.evgen.EVNT.e3898.VHbb.BETA_v06_MG_VJETS_0_Lep/     | sort -n > 361502.txt
#rucio list-files group.phys-higgs.mc15_13TeV.361501.MadGraphPythia8EvtGen_Zee_Np1.evgen.EVNT.e3898.VHbb.BETA_v06_MG_VJETS_0_Lep/     | sort -n > 361501.txt
#rucio list-files group.phys-higgs.mc15_13TeV.361500.MadGraphPythia8EvtGen_Zee_Np0.evgen.EVNT.e3898.VHbb.BETA_v06_MG_VJETS_0_Lep/     | sort -n > 361500.txt

#Z+Jets Sherpa 
#rucio list-files group.phys-higgs.mc15_13TeV.364155.Sherpa_NNPDF30NNLO_Znunu_MAXHTPTV1000_E_CMS.evgen.EVNT.e5308.VHbb.BETA_v06_VJETS_0_Lep/    | sort -n > 364155.txt
#rucio list-files group.phys-higgs.mc15_13TeV.364154.Sherpa_NNPDF30NNLO_Znunu_MAXHTPTV500_1000.evgen.EVNT.e5308.VHbb.BETA_v06_VJETS_0_Lep/      | sort -n > 364154.txt
#rucio list-files group.phys-higgs.mc15_13TeV.364153.Sherpa_Znunu_MAXHTPTV280_500_BFilter.evgen.EVNT.e5308.VHbb.BETA_v06_VJETS_0_Lep/           | sort -n > 364153.txt
#rucio list-files group.phys-higgs.mc15_13TeV.364152.Sherpa_Znunu_MAXHTPTV280_500_CFilterBVeto.evgen.EVNT.e5308.VHbb.BETA_v06_VJETS_0_Lep/      | sort -n > 364152.txt
#rucio list-files group.phys-higgs.mc15_13TeV.364150.Sherpa_Znunu_MAXHTPTV140_280_BFilter.evgen.EVNT.e5308.VHbb.BETA_v06_VJETS_0_Lep/           | sort -n > 364150.txt
#rucio list-files group.phys-higgs.mc15_13TeV.364149.Sherpa_Znunu_MAXHTPTV140_280_CFilterBVeto.evgen.EVNT.e5308.VHbb.BETA_v06_VJETS_0_Lep/      | sort -n > 364149.txt
#rucio list-files group.phys-higgs.mc15_13TeV.364147.Sherpa_Znunu_MAXHTPTV70_140_BFilter.evgen.EVNT.e5308.VHbb.BETA_v06_VJETS_0_Lep/            | sort -n > 364147.txt
#rucio list-files group.phys-higgs.mc15_13TeV.364146.Sherpa_Znunu_MAXHTPTV70_140_CFilterBVeto.evgen.EVNT.e5308.VHbb.BETA_v06_VJETS_0_Lep/       | sort -n > 364146.txt
#rucio list-files group.phys-higgs.mc15_13TeV.364144.Sherpa_Znunu_MAXHTPTV0_70_BFilter.evgen.EVNT.e5308.VHbb.BETA_v06_VJETS_0_Lep/              | sort -n > 364144.txt
#rucio list-files group.phys-higgs.mc15_13TeV.364143.Sherpa_Znunu_MAXHTPTV0_70_CFilterBVeto.evgen.EVNT.e5308.VHbb.BETA_v06_VJETS_0_Lep/         | sort -n > 364143.txt
#rucio list-files group.phys-higgs.mc15_13TeV.364141.Sherpa_Ztautau_MAXHTPTV1000_E_CMS.evgen.EVNT.e5307.VHbb.BETA_v06_VJETS_0_Lep/              | sort -n > 364141.txt
#rucio list-files group.phys-higgs.mc15_13TeV.364140.Sherpa_NNPDF30NNLO_Ztautau_MAXHTPTV500_1000.evgen.EVNT.e5307.VHbb.BETA_v06_VJETS_0_Lep/    | sort -n > 364140.txt
#rucio list-files group.phys-higgs.mc15_13TeV.364139.Sherpa_Ztautau_MAXHTPTV280_500_BFilter.evgen.EVNT.e5313.VHbb.BETA_v06_VJETS_0_Lep/         | sort -n > 364139.txt
#rucio list-files group.phys-higgs.mc15_13TeV.364138.Sherpa_Ztautau_MAXHTPTV280_500_CFilterBVeto.evgen.EVNT.e5313.VHbb.BETA_v06_VJETS_0_Lep/    | sort -n > 364138.txt
#rucio list-files group.phys-higgs.mc15_13TeV.364136.Sherpa_Ztautau_MAXHTPTV140_280_BFilter.evgen.EVNT.e5307.VHbb.BETA_v06_VJETS_0_Lep/         | sort -n > 364136.txt
#rucio list-files group.phys-higgs.mc15_13TeV.364135.Sherpa_Ztautau_MAXHTPTV140_280_CFilterBVeto.evgen.EVNT.e5307.VHbb.BETA_v06_VJETS_0_Lep/    | sort -n > 364135.txt
#rucio list-files group.phys-higgs.mc15_13TeV.364133.Sherpa_Ztautau_MAXHTPTV70_140_BFilter.evgen.EVNT.e5307.VHbb.BETA_v06_VJETS_0_Lep/          | sort -n > 364133.txt
#rucio list-files group.phys-higgs.mc15_13TeV.364132.Sherpa_Ztautau_MAXHTPTV70_140_CFilterBVeto.evgen.EVNT.e5307.VHbb.BETA_v06_VJETS_0_Lep/     | sort -n > 364132.txt
#rucio list-files group.phys-higgs.mc15_13TeV.364130.Sherpa_Ztautau_MAXHTPTV0_70_BFilter.evgen.EVNT.e5307.VHbb.BETA_v06_VJETS_0_Lep/            | sort -n > 364130.txt
#rucio list-files group.phys-higgs.mc15_13TeV.364129.Sherpa_Ztautau_MAXHTPTV0_70_CFilterBVeto.evgen.EVNT.e5307.VHbb.BETA_v06_VJETS_0_Lep/       | sort -n > 364129.txt
#rucio list-files group.phys-higgs.mc15_13TeV.364127.Sherpa_NNPDF30NNLO_Zee_MAXHTPTV1000_E_CMS.evgen.EVNT.e5299.VHbb.BETA_v06_VJETS_0_Lep/      | sort -n > 364127.txt
#rucio list-files group.phys-higgs.mc15_13TeV.364126.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV500_1000.evgen.EVNT.e5299.VHbb.BETA_v06_VJETS_0_Lep/    | sort -n > 364126.txt
#rucio list-files group.phys-higgs.mc15_13TeV.364125.Sherpa_Zee_MAXHTPTV280_500_BFilter.evgen.EVNT.e5299.VHbb.BETA_v06_VJETS_0_Lep/             | sort -n > 364125.txt
#rucio list-files group.phys-higgs.mc15_13TeV.364124.Sherpa_Zee_MAXHTPTV280_500_CFilterBVeto.evgen.EVNT.e5299.VHbb.BETA_v06_VJETS_0_Lep/        | sort -n > 364124.txt
#rucio list-files group.phys-higgs.mc15_13TeV.364122.Sherpa_Zee_MAXHTPTV140_280_BFilter.evgen.EVNT.e5299.VHbb.BETA_v06_VJETS_0_Lep/             | sort -n > 364122.txt
#rucio list-files group.phys-higgs.mc15_13TeV.364121.Sherpa_Zee_MAXHTPTV140_280_CFilterBVeto.evgen.EVNT.e5299.VHbb.BETA_v06_VJETS_0_Lep/        | sort -n > 364121.txt
#rucio list-files group.phys-higgs.mc15_13TeV.364119.Sherpa_Zee_MAXHTPTV70_140_BFilter.evgen.EVNT.e5299.VHbb.BETA_v06_VJETS_0_Lep/              | sort -n > 364119.txt
#rucio list-files group.phys-higgs.mc15_13TeV.364118.Sherpa_Zee_MAXHTPTV70_140_CFilterBVeto.evgen.EVNT.e5299.VHbb.BETA_v06_VJETS_0_Lep/         | sort -n > 364118.txt
#rucio list-files group.phys-higgs.mc15_13TeV.364116.Sherpa_NNPDF30NNLO_Zee_MAXHTPTV0_70_BFilter.evgen.EVNT.e5299.VHbb.BETA_v06_VJETS_0_Lep/    | sort -n > 364116.txt
#rucio list-files group.phys-higgs.mc15_13TeV.364115.Sherpa_Zee_MAXHTPTV0_70_CFilterBVeto.evgen.EVNT.e5299.VHbb.BETA_v06_VJETS_0_Lep/           | sort -n > 364145.txt
#rucio list-files group.phys-higgs.mc15_13TeV.364113.Sherpa_NNPDF30NNLO_Zmumu_MAXHTPTV1000_E_CMS.evgen.EVNT.e5271.VHbb.BETA_v06_VJETS_0_Lep/    | sort -n > 364113.txt
#rucio list-files group.phys-higgs.mc15_13TeV.364112.Sherpa_NNPDF30NNLO_Zmumu_MAXHTPTV500_1000.evgen.EVNT.e5271.VHbb.BETA_v06_VJETS_0_Lep/      | sort -n > 364112.txt
#rucio list-files group.phys-higgs.mc15_13TeV.364111.Sherpa_Zmumu_MAXHTPTV280_500_BFilter.evgen.EVNT.e5271.VHbb.BETA_v06_VJETS_0_Lep/           | sort -n > 364111.txt
#rucio list-files group.phys-higgs.mc15_13TeV.364110.Sherpa_Zmumu_MAXHTPTV280_500_CFilterBVeto.evgen.EVNT.e5271.VHbb.BETA_v06_VJETS_0_Lep/      | sort -n > 364110.txt
#rucio list-files group.phys-higgs.mc15_13TeV.364108.Sherpa_Zmumu_MAXHTPTV140_280_BFilter.evgen.EVNT.e5271.VHbb.BETA_v06_VJETS_0_Lep/           | sort -n > 364108.txt
#rucio list-files group.phys-higgs.mc15_13TeV.364107.Sherpa_Zmumu_MAXHTPTV140_280_CFilterBVeto.evgen.EVNT.e5271.VHbb.BETA_v06_VJETS_0_Lep/      | sort -n > 364107.txt
#rucio list-files group.phys-higgs.mc15_13TeV.364105.Sherpa_Zmumu_MAXHTPTV70_140_BFilter.evgen.EVNT.e5271.VHbb.BETA_v06_VJETS_0_Lep/            | sort -n > 364105.txt
#rucio list-files group.phys-higgs.mc15_13TeV.364104.Sherpa_Zmumu_MAXHTPTV70_140_CFilterBVeto.evgen.EVNT.e5271.VHbb.BETA_v06_VJETS_0_Lep/       | sort -n > 364104.txt
#rucio list-files group.phys-higgs.mc15_13TeV.364102.Sherpa_Zmumu_MAXHTPTV0_70_BFilter.evgen.EVNT.e5271.VHbb.BETA_v06_VJETS_0_Lep/              | sort -n > 364102.txt
#rucio list-files group.phys-higgs.mc15_13TeV.364101.Sherpa_Zmumu_MAXHTPTV0_70_CFilterBVeto.evgen.EVNT.e5271.VHbb.BETA_v06_VJETS_0_Lep/         | sort -n > 364101.txt
